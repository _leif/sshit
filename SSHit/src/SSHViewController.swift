//
//  SSHViewController.swift
//  SSHit
//
//  Created by Leif on 2/25/19.
//  Copyright © 2019 Zach Eriksen. All rights reserved.
//

import Cocoa


struct SSH {
    let user: String
    let url: String
    let password: String?
    
    init(user: String, url: String, password: String? = nil) {
        self.user = user
        self.url = url
        self.password = password
    }
}

let all: [SSH] = [
    SSH(user: "dummyUser_01", url: "localhost"),
    SSH(user: "dummyUser_02", url: "localhost"),
    SSH(user: "dummyUser_03", url: "39.478.87.219")
]

class SSHViewController: NSViewController {
    static let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
    static let ID = NSStoryboard.SceneIdentifier("SSHViewController")
    // MARK: Storyboard instantiation
    static func instantiate() -> SSHViewController {
        guard let vc = SSHViewController.storyboard.instantiateController(withIdentifier: SSHViewController.ID) as? SSHViewController else {
            fatalError("Could not find storyboard with ID: \(SSHViewController.ID)")
        }
        return vc
    }
    // MARK: Outlets
    @IBOutlet weak var sshLabel: NSTextField!
    
    var clients: [SSH] = all
    var currentClientIndex = 0 {
        didSet {
            updateClient()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentClientIndex = 0
    }
    
    func updateClient() {
        let client = clients[currentClientIndex]
        let labelText = "\(client.user)@\(client.url)"
        sshLabel.stringValue = labelText
    }
    
    // MARK: Actions
    
    @IBAction func back(_ sender: Any) {
        currentClientIndex = (currentClientIndex - 1 + clients.count) % clients.count
    }
    
    @IBAction func next(_ sender: Any) {
        currentClientIndex = (currentClientIndex + 1) % clients.count
    }
    
    @IBAction func ssh(_ sender: Any) {
        let client = clients[currentClientIndex]
        ssh(to: client.url, withUser: client.user)
    }
    
    private func ssh(to: String, withUser user: String) {
        let task = Process()
        task.launchPath = "/usr/bin/osascript"
        
        let cmd = "\"ssh \(to) -l '\(user)'\""
        
        task.arguments = ["-e", "tell application \"Terminal\"",
                          "-e", "activate",
                          "-e", "do script \(cmd)",
            "-e", "end tell"]
        
        task.launch()
    }

}
