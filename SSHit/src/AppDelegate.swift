//
//  AppDelegate.swift
//  SSHit
//
//  Created by Leif on 2/25/19.
//  Copyright © 2019 Zach Eriksen. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    // Popover
    let popover = NSPopover()
    let statusItem = NSStatusBar.system.statusItem(withLength:NSStatusItem.squareLength)
    var statusItemButton: NSStatusBarButton {
        guard let button = statusItem.button else {
            fatalError("Could not unwrap statusItem.button")
        }
        return button
    }
    // Monitor
    lazy var eventMonitor: EventMonitor = {
        return EventMonitor(mask: [.leftMouseDown, .rightMouseDown]) { [weak self] event in
            if self?.popover.isShown ?? false {
                self?.close(event)
            }
        }
    }()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        statusItemButton.image = NSImage(named:NSImage.Name("console"))
        statusItemButton.action = #selector(togglePopover)
        popover.contentViewController = SSHViewController.instantiate()
    }
    
    @objc
    private func togglePopover(_ sender: Any?) {
        popover.isShown ? close(sender) : show()
    }
    
    private func show() {
        popover.show(relativeTo: statusItemButton.bounds,
                     of: statusItemButton,
                     preferredEdge: NSRectEdge.minY)
        eventMonitor.start()
    }
    
    private func close(_ sender: Any?) {
        popover.performClose(sender)
        eventMonitor.stop()
    }
}

